package main 

import (
	"time"
	"server"
	"connpool"
)

func main() {
	server.Redis = connpool.MakePool("tcp", "localhost:6379", 500)
	ticker := time.NewTicker(time.Second * 20);

	server.GraphUpdateTask()
	for {
		select {
		case <- ticker.C:
			server.GraphUpdateTask()
		}
	}
}