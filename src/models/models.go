package models

type APIRequest struct {
	Relation string
	User1 string
	User2 string
	Topic string
	Tag string
}

type APIRequestList struct {
	Requests []APIRequest
}
