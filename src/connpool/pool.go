// Connection pool in go 

package connpool

import (
	"fmt"
	"github.com/fzzy/radix/redis"
)

type Pool struct {
	workers chan *redis.Client
	host string
	port string
	Hits int
	Releases int
}

func MakePool(host, port string, size int) Pool {
	p := Pool{host:host, port:port}
	p.Init(size)
	return p
}

func (p *Pool) Init (size int) {
	p.workers = make(chan *redis.Client, size)
	for i := 0; i < size; i++ {
		client, err := redis.Dial(p.host, p.port)
		if err != nil {
			fmt.Println(err)
			continue
		}
		p.workers <- client
	}
}

func (p *Pool) Get() *redis.Client {
	p.Hits++
	cl := <- p.workers
	return cl
}

func (p *Pool) Release(client *redis.Client) {
	p.Releases++
	p.workers <- client
}

func (p *Pool) Close() {
	close(p.workers)
	for worker := range p.workers {
		worker.Close()
	}
}