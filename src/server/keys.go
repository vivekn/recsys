// Describe all key structures here as keeping track of them may get unwieldy

package server

import (
	"fmt"
)


func ktopicset() string {
	return "UserSet"
}

func kuserset() string {
	return "TopicSet"
}

func ktopic(tid string) string {
	return fmt.Sprintf("Topic::%s", tid)
}

func kuser_tag_topics(uid, tag string) string {
	return fmt.Sprintf("User::%s.%s.topics", uid, tag)
}

func kuser_friends(uid string) string {
	return fmt.Sprintf("User::%s.friends", uid)
}

func kuser_tag_friends(uid, tag string) string {
	return fmt.Sprintf("User::%s.%s.friends", uid, tag)
}

func ktags() string {
	return "Topics"
}
