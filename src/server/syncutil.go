// Implementation of thread safe data structures for handling parallelism

package server

import (
	"sync"
)

type SyncSet struct {
	mu sync.Mutex
	m map[string]bool
}

func NewSyncSet() SyncSet{
	return SyncSet{m:make(map[string]bool)}
}

type SyncCtr struct {
	mu sync.Mutex
	m map[string]int
}

func (s *SyncCtr) Incr(key string) {
	s.mu.Lock()
	s.m[key] += 1
	s.mu.Unlock()
}

// Unsafe but faster increment
func (s *SyncCtr) FIncr(key string) {
	s.m[key] += 1
}

func NewSyncCtr() SyncCtr{
	return SyncCtr{m:make(map[string]int)}
}

type SyncMap struct {
	mu sync.Mutex
	m map[string]string
}

func NewSyncMap() SyncMap{
	return SyncMap{m:make(map[string]string)}
}

func (s *SyncMap) Get(key string) string {
	return s.m[key]
}

func (s *SyncMap) Set(key, val string) {
	s.mu.Lock()
	s.m[key] = val
	s.mu.Unlock()
}

// Structure for keeping track of active goroutines, 
// sends on the channel when there are no active
// goroutines tracked by the GTracker object.
type GTracker struct {
	C chan bool
	mu sync.Mutex
	ctr int
}

func NewTracker() GTracker {
	return GTracker{C:make(chan bool)}
}

func (g *GTracker) Incr() {
	g.mu.Lock()
	g.ctr += 1
	g.mu.Unlock()
} 

func (g *GTracker) Decr() {
	g.mu.Lock()
	g.ctr -= 1
	g.mu.Unlock()

	if 	g.ctr == 0 {
		g.C <- true
	}
}