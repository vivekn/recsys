package server

import (
	"time"
	"sort"
	"math"
)

// Recommendation algorithm that suggests topics from friends
// topics, weighting based on similarity of tastes
func mutualLikes(userId string, limit int, tag string) []string{
	friends := getFriends(userId)
	suggestions := make(map[string]int)
	myTopics := getUserTopics(userId, tag)
	ch := make(chan map[string]int)

	for _, friend := range friends {
		go compareFriend(friend, tag, myTopics, ch)
	}

	timeout := time.After(time.Second * 10)

	for i:=0; i < len(friends); i++ {
		select {
		case m := <-ch :
			suggestions = addMaps(suggestions, m)
		case <-timeout:
			break
		}
	}
	sugpl := toPairList(suggestions)
	sort.Sort(sugpl)

	res := make([]string, limit)

	for i := 0 ; i < len(sugpl) && i < limit; i++ {
		res[i] = sugpl[i].Key
	}

	return res
}
 
func compareFriend(friend, tag string, myTopics map[string]int, c chan map[string]int) {
	topics := getUserTopics(friend, tag)
	similarity := int(math.Pow(float64(dotMaps(myTopics, topics)), 2))
	suggest := scaleMap(addMaps(topics, scaleMap(myTopics, -1)), similarity)
	c <- suggest
}