math.randomseed(ARGV[1])
local k = tonumber(ARGV[2])
local n = tonumber(redis.call("hlen", KEYS[1]))
local sum = tonumber(redis.call("hget", KEYS[2], n))
local t = {}
for i=1, k do
	local key = math.random() * sum
	local lo, hi = -1, n
	while lo + 1 < hi do
		local p = math.floor((lo + hi) / 2)
		local val = tonumber(redis.call("hget", KEYS[2], p))
		if (val > key) then
			hi = p
		else 
			lo = p
		end
	end 
	local res = redis.call("HGET", KEYS[1], lo)
	table.insert(t, res)
end
return t