package server

import (
	"time"
	"sort"
	"github.com/spf13/nitro"
)

var nitroTimer *nitro.B

const (
	TOPICS = iota
	USERS
)

type Node struct {
	Id string
	Type int
	TTL int
}

type Prop struct { // Pass around state information
	mode int
	tag string
	track *GTracker
	ctr *SyncCtr
}

func NewProp(mode int, tag string) Prop {
	tr, c := NewTracker(), NewSyncCtr()
	return Prop{mode: mode, tag: tag, track: &tr, ctr: &c}
}


func VisitGraph(userId, tag string, limit, mode int) []string {
	// Traverse graph with a fixed TTL, choosing a random subset
	// of edges at each step. This subset is defined by a probability
	// distribution
	nitroTimer = nitro.Initialize()

	friends := getFriendsSample(userId, tag)
	topics  := getTopicsSample(userId, tag)	
	p := NewProp(mode, tag)
	ttl := 2

	for _, friend := range friends {
		go visitNode(Node{friend, USERS, ttl}, &p)
	}

	for _, topic := range topics {
		go visitNode(Node{topic, TOPICS, ttl}, &p)
	}

	timeout := time.After(time.Second * 30)
	
	// ensure all go routines have completed.
	select {
	case <- timeout: 
	case <- p.track.C:
	}

	// sort keys by value
	sugpl := toPairList(p.ctr.m)
	sort.Sort(sugpl)

	res := make([]string, limit)
	myTopics := getUserTopics(userId, tag)

	for i, j := 0, 0 ; i < len(sugpl) && j < limit; i++ {
		// Ensure recommendation is not already in users' list
		if _, ok := myTopics[sugpl[i].Key]; !ok {
			res[j] = sugpl[i].Key
			j++
		}
	}

	return res
}


// Simple parallel dfs implementation
func visitNode(nd Node, prop *Prop) {
	prop.track.Incr()

	if (nd.Type == prop.mode) {
		prop.ctr.Incr(nd.Id)
	}

	if (nd.TTL > 0) {
		switch(nd.Type) {
		case TOPICS:
			users := getUsersSample(nd.Id)
			for _, user := range users {
				go visitNode(Node{user, USERS, nd.TTL - 1}, prop)
			}
		case USERS:
			friends := getFriendsSample(nd.Id, prop.tag)
			topics  := getTopicsSample(nd.Id, prop.tag)	

			for _, friend := range friends {
				go visitNode(Node{friend, USERS, nd.TTL - 1}, prop)
			}

			for _, topic := range topics {
				go visitNode(Node{topic, TOPICS, nd.TTL - 1}, prop)
			}
		}
	}

	prop.track.Decr()	
}

// Sequential visit node implementation
func seqVisitNode(nd Node, prop *Prop) {

	if (nd.Type == prop.mode) {
		prop.ctr.FIncr(nd.Id)
	}

	if (nd.TTL > 0) {
		switch(nd.Type) {
		case TOPICS:
			users := getUsersSample(nd.Id)
			for _, user := range users {
				seqVisitNode(Node{user, USERS, nd.TTL - 1}, prop)
			}
		case USERS:
			friends := getFriendsSample(nd.Id, prop.tag)
			topics  := getTopicsSample(nd.Id, prop.tag)	

			for _, friend := range friends {
				seqVisitNode(Node{friend, USERS, nd.TTL - 1}, prop)
			}

			for _, topic := range topics {
				seqVisitNode(Node{topic, TOPICS, nd.TTL - 1}, prop)
			}
		}
	}

}
