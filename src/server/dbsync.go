// Contains database entry points and connection configuration 

package server

import (
	"models"
	"fmt"
	"os"
	"github.com/fzzy/radix/redis"
	"connpool"
)

// Thread safe local list of all tags
var localtags SyncSet = NewSyncSet()
var Redis connpool.Pool

func errHndlr(err error) {
	if err != nil {
		fmt.Println("err: ", err)
		os.Exit(1)
	}
}

func connect() *redis.Client {
	c, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		fmt.Println("Connection failed")
		errHndlr(err)
	}
	return c
}

func InitTags() { // update local list of tags
	c := Redis.Get()
	tags, err := c.Cmd("smembers", ktags()).List()
	errHndlr(err)
	localtags.mu.Lock()
	for _, tag := range tags {
		localtags.m[tag] = true
	}
	localtags.mu.Unlock()
	defer Redis.Release(c)
}

func checkTag(tag string) {
	// Check if tag exists in global list of tags, if not create it
	if !localtags.m[tag] {
		localtags.mu.Lock()
		localtags.m[tag] = true
		c := Redis.Get()
		c.Cmd("sadd", ktags(), tag)
		localtags.mu.Unlock()
		Redis.Release(c)
	}
}

func getTags() [] string {
	
	if len(localtags.m) == 0 {
		InitTags()
	}	

	localtags.mu.Lock() 

	tags := make([]string, len(localtags.m))
	i := 0
	for k, _ := range localtags.m {
		tags[i] = k
		i++
	}

	localtags.mu.Unlock()

	return tags
}

// API entry point for adding data to db
func ProcessStoreRequests(reqs *models.APIRequestList) {
	c := Redis.Get()

	for _, req := range reqs.Requests {
		switch(req.Relation) {
		case "user_like":
			likesPath := kuser_tag_topics(req.User1, req.Tag)
			topicPath := ktopic(req.Topic)
			c.Append("sadd", likesPath, req.Topic)
			c.Append("sadd", topicPath, req.User1)
			c.Append("sadd", ktopicset(), req.Topic)
			c.Append("sadd", kuserset(), req.User1)
			checkTag(req.Tag)
		case "friends":
			fPath1 := kuser_friends(req.User1)
			fPath2 := kuser_friends(req.User2)
			c.Append("sadd", fPath1, req.User2)
			c.Append("sadd", fPath2, req.User1)
			c.Append("sadd", kuserset(), req.User1)
			c.Append("sadd", kuserset(), req.User2)
		}		
	}

	c.GetReply()
	Redis.Release(c)
}

func GetRecommendations(userId string, limit int, tag string) []string {
	// return mutualLikes(userId, limit, tag)
	return VisitGraph(userId, tag, limit, TOPICS)
}

func getFriends(userId string) []string {
	c := Redis.Get()
	path := kuser_friends(userId) 
	res, err := c.Cmd("smembers", path).List()
	errHndlr(err)
	defer Redis.Release(c)
	return res
}

func getUserTopics(userId, tag string) map[string]int {
	c := Redis.Get()
	path := kuser_tag_topics(userId, tag)
	res, err := c.Cmd("smembers", path).List()
	errHndlr(err)
	
	topicMap := make(map[string]int)
	for _, topic := range res  {
		topicMap[topic] = 1
	}
	defer Redis.Release(c)
	return topicMap
}

func getAllUsers() []string{
	c := Redis.Get()
	users, err := c.Cmd("smembers", kuserset()).List()
	errHndlr(err)
	defer Redis.Release(c)
	return users
}
