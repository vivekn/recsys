// Weighted sampling of arrays stored in a redis db

package server

import (
	"time"
	"github.com/fzzy/radix/redis"
	"strconv"
)

const K = 30


// Lua script for weighted sampling
const KSample string = `
math.randomseed(ARGV[1])
local k = tonumber(ARGV[2])
local n = tonumber(redis.call("hlen", KEYS[1]))
local sum = tonumber(redis.call("hget", KEYS[2], n))
if (sum == nil) then
	return redis.error_reply(KEYS[2] .. " " .. tostring(n))
end
local t = {}
for i=1, k do
	local key = math.random() * sum
	local lo, hi = -1, n
	while lo + 1 < hi do
		local p = math.floor((lo + hi) / 2)
		local val = tonumber(redis.call("hget", KEYS[2], p))
		if (val > key) then
			hi = p
		else 
			lo = p
		end
	end 
	local res = redis.call("HGET", KEYS[1], lo)
	table.insert(t, res)
end
return t`

// Store hashes of scripts in a thread safe way
var scripts SyncMap = NewSyncMap()

func ExecScript(script string, keys, args []string) *redis.Reply {
	c := Redis.Get()
	var r *redis.Reply
	if sha := scripts.Get(script); len(sha) > 0 {
		pack := make([]interface{}, 0, 10)
		pack = append(pack, sha)
		pack = append(pack, len(keys))
		for _, x := range keys {
			pack = append(pack, x)
		}
		for _, x := range args {
			pack = append(pack, x)
		}
		r = c.Cmd("EVALSHA", pack...)
	} else {
		sha = c.Cmd("SCRIPT", "LOAD", script).String()
		scripts.Set(script, sha)
		pack := make([]interface{}, 0, 10)
		pack = append(pack, sha)
		pack = append(pack, len(keys))
		for _, x := range keys {
			pack = append(pack, x)
		}
		for _, x := range args {
			pack = append(pack, x)
		}
		r = c.Cmd("EVALSHA", pack...)
	}
	Redis.Release(c)
	return r
}

func Sampler(prefix string) []string {
	// Uses prefix as the namespace
	// $(prefix).i stores the keys of the array to be sampled
	// $(prefix).w stores the cumulative sum of unnormalised probabilities upto 
	// that particular index
	keys := []string{prefix + ".i", prefix+".w"}
	//seed with current system time
	args := []string{strconv.Itoa(time.Now().Nanosecond()), strconv.Itoa(K)}
	r, e := ExecScript(KSample, keys, args).List()
	errHndlr(e)
	return r
}

func getTopicsSample(userId, tag string) []string {
	prefix := kuser_tag_topics(userId, tag)
	return Sampler(prefix)
}

func getUsersSample(topicId string) []string {
	c := Redis.Get()
	sample, e := c.Cmd("srandmember", ktopic(topicId), K).List()
	errHndlr(e)
	defer Redis.Release(c)
	return sample
}

func getFriendsSample(userId, tag string) []string {
	prefix := kuser_tag_friends(userId, tag)
	return Sampler(prefix)
}

