// Set of utility functions for dealing with maps

package server

func dotMaps(a, b map[string]int) int {
	var ans int = 0
	for k, v := range a {
		ans += v * b[k]
	}
	return ans
}


func fdotMaps(a, b map[string]float64) float64 {
	var ans float64 = 0
	for k, v := range a {
		ans += v * b[k]
	}
	return ans
}

func addMaps(a, b map[string]int) map[string]int {
	c := make(map[string]int)
	for k, v := range a {
		if v != 0 {
			c[k] = v
		}
	}
	for k, v := range b {
		if v != 0 {
			c[k] += v
		}
	}
	return c
}

func scaleMap(m map[string]int, alpha int) map[string]int {
	res := make(map[string]int)
	for k, v := range m {
		if alpha*v != 0 {
			res[k] = alpha * v
		}
	}
	return res
}

func absMap(m map[string]int) map[string]int {
	res := make(map[string]int)
	for k, v := range m {
		if v < 0 {
			res[k] = -v
		} else {
			res[k] = v
		}
	}
	return res	
}

func apply(m map[string]int, f func (int) float64) map[string]float64 {
	res := make(map[string]float64)
	for k, v := range m {
		res[k] = f(v)
	}
	return res
}

func applyKey(m map[string]int, f func (string) float64) map[string]float64 {
	res := make(map[string]float64)
	for k, _ := range m {
		res[k] = f(k)
	}
	return res
}


var AddMaps func (map[string]int, map[string]int) map[string]int = addMaps
var ScaleMap func (map[string]int, int) map[string]int = scaleMap
var AbsMap func (map[string]int) map[string]int = absMap
