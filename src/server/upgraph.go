package server

import (
	"fmt"
)

//periodic weights update of graphedges

var topicScore map[string]float64 = map[string]float64{}

func LoadTopics() {
	c := connect()
	keys, err := c.Cmd("smembers", ktopicset()).List()
	errHndlr(err)
	// TODO: Fast multi scard
	for _, key := range keys {
		card, _ := c.Cmd("scard", ktopic(key)).Int()
		topicScore[key] = invPop(card)
	}
}

func invPop(numItems int) float64 { // inverse popularity of topic
	return (1.0 / float64(numItems))
}

func writeWeightsHash(key string, weights map[string]float64) {
	// Key Namespace : `key`.w and `key`.i - two separate hashes
	i := 1
	c := Redis.Get()
	c.Cmd("hset", key + ".w", 0, 0) // set first ele to zero
	var cumWt float64 = 0


	for k, v := range weights {
		cumWt += v // store cumulative sum to make random sampling faster
		c.Cmd("hset", key + ".i", i-1, k) // id (i-1) represents weight between w[i-1] to w[i]
		c.Cmd("hset", key + ".w", i, cumWt)
		i++
	}

	Redis.Release(c)

}

func updateUser(c chan bool, userId string)  {
	tags := getTags()
	friends := getFriends(userId)
	f := func(x string) float64 {return topicScore[x]}

	for _, tag := range tags {
		// For each tag, recalculate user-topic and user-user weights
		userTopics := getUserTopics(userId, tag)
		weights := applyKey(userTopics, f)	
		uwKey := kuser_tag_topics(userId, tag)
		writeWeightsHash(uwKey, weights)
		fweights := make(map[string]float64)

		for _, friend := range friends { 
			// For user-user weights, use dot product of inverse popularities as a metric
			friendTopics := getUserTopics(friend, tag)
			fvec := applyKey(friendTopics, f)
			fweights[friend] = fdotMaps(fvec, weights)
		}
		fwKey := kuser_tag_friends(userId, tag)
		writeWeightsHash(fwKey, fweights)
	}
	c <- true
}


// Entry point for update task
func GraphUpdateTask() {
	fmt.Println("Starting to update graph")
	LoadTopics()
	InitTags()
	users := getAllUsers()
	c := make(chan bool)

	for i, user := range users {
		fmt.Printf("Starting %d\n", i)
		go updateUser(c, user)
	}

	// Ensure all goroutines finish executing
	for i:=0; i < len(users); i++ {
		<- c
		fmt.Printf("%d of %d users completed\n", i+1, len(users))
	}
	fmt.Println("Completed updating weights")

 }
