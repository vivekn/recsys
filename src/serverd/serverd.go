package main

import (
	"fmt"
	"strconv"
	"net/http"
	"encoding/json"
	"server"
	"models"
	"connpool"
	"flag"
)

func store(w http.ResponseWriter, r *http.Request) {
	var reqlist models.APIRequestList
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&reqlist)
	server.ProcessStoreRequests(&reqlist)
}

func recommend(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := r.FormValue("user")
	limit, e := strconv.Atoi(r.FormValue("limit"))
	if e != nil {
		limit = 20
	}
	tag := r.FormValue("tag")
	bytes, _ := json.Marshal(server.GetRecommendations(user, limit, tag))
	text := string(bytes)
	fmt.Println(text)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, text)
}

func main() {
	flag.Parse()
	server.Redis = connpool.MakePool("tcp", "localhost:6379", 100)
	server.InitTags()
	http.HandleFunc("/store/", store)
	http.HandleFunc("/get/", recommend)
	http.ListenAndServe(":8000", nil)
}