from fb import f
from simplejson import loads, dumps
import requests
import time
import eventlet


SERVER='http://127.0.0.1:8000'

names = {}

class RequestList:
	def __init__(self, path=""):
		self.d = {"Requests":[]}
		if len(path) > 0:
			self.d = loads(open(path).read())

	def add_friend(self, user1, user2):
		self.d["Requests"].append({
			"Relation": "friends",
			"User1": user1,
			"User2": user2
			})

	def add_like(self, user, topic, tag=""):
		self.d["Requests"].append({
			"Relation": "user_like",
			"User1": user,
			"Topic": topic,
			"Tag": tag
			})

	def send(self, send_data=True, path="dump"):
		s = dumps(self.d)
		open(path, "w").write(s)
		if send_data:
			requests.post(SERVER + '/store/', s)

def fetch(friend):
	res = []
	for movie in f.movies(friend):
		res.append((friend, movie['id'], 'movie'))
		names[movie['id']] = movie['name']
	return res, friend


def simple_test():
	print "Getting friends"
	friends = f.friends()
	reqs = RequestList()
	myid = f.get_platform_id()
	# left = 2
	pool = eventlet.GreenPool()
	for res, friend in map(fetch, friends):
		for item in res:
			reqs.add_like(*item)
		reqs.add_friend(friend, myid)
	
	print "Adding my stuff"
	for movie in f.movies():
		reqs.add_like(f.get_platform_id(), movie['id'], 'movie')
		names[movie['id']] = movie['name']

	print "Sending to go server"
	reqs.send(path='fbdump')

	# print "Fetching recos\n"
	# recos = loads(requests.get(SERVER + '/get/?user=%s&limit=%s&tag=%s' % (f.me()['id'], 50*left, 'movie')).text)

	# print "Leave %d out test" % left
	# print "Matches: ", len(set(m['id'] for m in f.movies()[-left:]) & set(recos))

if __name__ == '__main__':
	reqs = RequestList('fbdump')
	reqs.send()
	# simple_test()
	# f = open("names", "w")
	# f.write(dumps(names))
	names = loads(open("names", "r").read())
	recos = loads(requests.get(SERVER + '/get/?user=%d&limit=%d&tag=%s' % (809009796, 30, 'movie')).text)
	print [names[reco] for reco in recos]


