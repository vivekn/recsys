'''
Facebook Wrapper for Facebook
'''
from simplejson import loads, dumps
from eventlet.green import urllib2


class Facebook(object):

    def __init__(self, auth_token):
        self.auth_token = auth_token

    def get_platform_id(self):
        return self.me()['id']

    def load(self, method, user_id = 'me'):
        raw = urllib2.urlopen("https://graph.facebook.com/%s/%s/?access_token=%s" % (user_id, method, self.auth_token)).read()
        data = loads(raw)
        if 'data' not in data:
            raise KeyError(str(data))
        return data['data'] 

    def with_fields(self, method, user_id = 'me', fields = 'name,likes'):
        raw = urllib2.urlopen("https://graph.facebook.com/%s/%s/?fields=%s&access_token=%s" % (user_id, method, fields, self.auth_token)).read()
        data = loads(raw)
        if 'data' not in data:
            raise KeyError(str(data))
        return data['data'] 

    def likes(self, user_id = 'me'):
        return self.with_fields('likes', user_id, 'name,category')

    def me(self, user_id='me'):
        data = loads (urllib2.urlopen("https://graph.facebook.com/%s?fields=name&access_token=%s" % (user_id, self.auth_token)).read())
        return data or {}

    def expand(self, like):
        data = loads (urllib2.urlopen("https://graph.facebook.com/%s?access_token=%s" % (like['id'], self.auth_token)).read())
        return data or {}

    def friends(self, user_id = 'me'):
        return [x['id'] for x in self.load('friends', user_id)]

    def _friends(self, user_id = 'me'):
        return self.load('friends', user_id)

    def movies(self, user_id = 'me'):
        return self.with_fields('movies', user_id)

    def music(self, user_id = 'me'):
        return self.with_fields('music', user_id)

    def books(self, user_id = 'me'):
        return self.with_fields('books', user_id)

    def picture(self, user_id='me', size=None):
        if size:
            return "https://graph.facebook.com/%s/picture?access_token=%s&type=%s" % (user_id, self.auth_token, size)
        return "https://graph.facebook.com/%s/picture?access_token=%s" % (user_id, self.auth_token)

    def search_feed(self, term):
        params = {
            'q': term,
            'access_token': self.auth_token
        }
        results = loads(session.request('GET', "https://graph.facebook.com/me/home", params=params).read())['data'] or []
        return results

    def names_from_ids(self, ids):
        friends = self._friends()
        names = dict((friend['id'], friend['name']) for friend in friends)
        return names

f = Facebook('CAACEdEose0cBAIkKlaSlAUJh2oYG5uCPnMIhI9cZB9fJT79VimUpMyt6fHhtFtC0xp3IFwm0qTVLBJJZAVHTXZAhTIX73tCX5Qoh4ohoayuso4bsZCX54Aou7yvp05Tst8vKRYQxxBnqynGcjy6WziZA3zsYwos7FIZCeQujuKFlRk86nksSMOoMx71Heq9eLitubuE7ZChyQZDZD')

